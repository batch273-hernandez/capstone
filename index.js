class Customer {
	constructor(email) {		
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut() {
		if (this.cart.length !== 0) {

			const order = {
				products: this.cart.contents,
				totalAmount: this.cart.totalAmount
			}

			this.orders.push(order);
			// this.cart.clearCartContents();
			return this;
		}
	}
}

class Cart {
	constructor() {
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(product, quantity) {
		if (product.isActive == true) {
			this.contents.push({ product, quantity })
		} else {
			console.log("Product is unavailable.")
		}
		return this;
	}

	showCartContents() {
		this.contents;
		return this;
	}

	updateProductQuantity(name, newQuantity) {
		let productIndex = this.contents.findIndex(item => item.product.name === name);
		if(productIndex === -1) {
			console.log("Product not found!")
		}
		this.contents[productIndex].quantity = newQuantity;
		return this;
	}

	clearCartContents() {
		this.contents = [];
		this.totalAmount = 0;
		return this;
	}

	computeTotal() {
		let amount = 0;
		this.contents.forEach((item) => {
			amount += item.product.price * item.quantity;
		})
		this.totalAmount = amount;
		return this;
	}
}

class Product {
	constructor(name, price) {
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive() {
		if(this.isActive = true){
			this.isActive = false;
		}
		return this;
	}

	updatePrice(newPrice) {
		this.price = newPrice;
		return this;
	}
}


//-----------for testing--------------//
// const john = new Customer("john@mail.com");
// const prodA = new Product("soap", 9.99);
